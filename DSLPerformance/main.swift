//
//  main.swift
//  DSLPerformance
//
//  Created by vikingosegundo on 18.05.24.
//

import Foundation

enum DSL {
    case up(Value); enum Value {
        case value(Int)
    }
}

func a(_ i:Int) { }
func b(_ d:DSL) { }

var startA:Date!
var startB:Date!
var endA:Date!
var endB:Date!
var aFaster = 0
var bFaster = 0

let loops = 100
let runs = 1_000_000

func aTest() { startA = .now; (0..<runs).forEach { a($0)              }; endA = .now }
func bTest() { startB = .now; (0..<runs).forEach { b(.up(.value($0))) }; endB = .now }

let numberFormatterSeconds = NumberFormatter()
numberFormatterSeconds.numberStyle = .decimal
numberFormatterSeconds.maximumFractionDigits = 15
numberFormatterSeconds.minimumFractionDigits = 15
numberFormatterSeconds.locale = Locale(identifier:"en_US")

let numberFormatterRuns = NumberFormatter()
numberFormatterRuns.numberStyle = .decimal
numberFormatterRuns.locale = Locale(identifier:"en_US")

(1...loops).forEach {
    $0 % 2 == 0
        ? {aTest(); bTest()}()
        : {bTest(); aTest()}()
    
    let secondsA = endA.timeIntervalSince1970 - startA.timeIntervalSince1970
    let secondsB = endB.timeIntervalSince1970 - startB.timeIntervalSince1970
    if secondsA < secondsB {aFaster += 1} else {bFaster += 1}

    print("loop \($0) of \(loops)")
    print("Int: seconds for \(numberFormatterRuns.string(from: NSNumber(value: runs))!) runs: \(numberFormatterSeconds.string(from: NSNumber(value:secondsA))!)\tseconds for 1 run: \(secondsA/Double(runs))")
    print("DSL: seconds for \(numberFormatterRuns.string(from: NSNumber(value: runs))!) runs: \(numberFormatterSeconds.string(from: NSNumber(value:secondsB))!)\tseconds for 1 run: \(secondsB/Double(runs))")
    print(secondsA < secondsB ? "Int function call faster":"DSL function call faster")
    print("\n=========================================================================================")
}

print("Int function call was faster \(aFaster) times")
print("DSL function call was faster \(bFaster) times")
print("\n")
